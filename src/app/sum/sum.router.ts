import { Router } from "express";

export const router = Router();

router.get("/sum/:a/:b", (req, res) => {
  // TODO
  // This line is needed to run tests properly
  let a = parseInt(req.params.a);
  let b = parseInt(req.params.b);

  if (a != undefined && b != undefined) {
    if (!isNaN(a) && !isNaN(b)) {
      res.status(200).send({ msg: `Sum: ${a + b}` });
    } else {
      res.status(200).send({ msg: `Sum: ${"NaN"}` });
    }
  } else {
    res.status(400).send({ msg: `You have to edit this file!` });
  }
});
