# Praktikum 4

# Schritt 1
In Hinblick auf das anstehende fünfte Praktikum nutzen wir das offizielle gitlab.com GitLab für die Bearbeitung des folgenden Projekts. Erstellen Sie bitte - falls noch nicht vorhanden - einen eigenen Account.
Arbeiten Sie die folgenden Schritte ab:

Rufen Sie die Seite zum Import eines Projekts auf: https://gitlab.com/projects/new#import_project

Wählen Sie Repository by URL aus
Tragen Sie folgende URL in das Feld Git repository URL ein: https://gitlab.hsrw.eu/lv-swe/praktika/praktikum-iv-vorlage.git

Setzen Sie kein Häkchen bei Mirror repository

Da das zu importierende Projekt nicht öffentlich ist, müssen Sie Ihre Zugangsdaten zum Hochschul-GitLab angeben
Vergeben Sie einen geeigneten Namen und Ihre gewünschte Projektsichtbarkeit
Erstellen Sie das Projekt durch den Button Create project


Alle weiteren Schritte erarbeiten Sie bitte in Ihrem persönlichen Projekt. Denken Sie daran, dass Sie den GitLab-Flow aus dem letzten Praktikum benutzen sollen, um die Arbeitsweise zu verinnerlichen.

# Schritt 2
Unter /src/app/sum/sum.router.ts finden Sie eine unvollständige TypeScript-Datei vor. Wir wollen in diesem Praktikum testgetrieben mit Hilfe einer Gitlab-Pipeline die Lösung erarbeiten. Die Tests wurden bereits unter /src/app/sum/sum.test.ts erstellt, hier müssen Sie keine weiteren Änderungen vornehmen.
Im Hauptverzeichnis finden Sie bereits eine rudimentäre .gitlab-ci.yml vor. Sie werden diese in den folgenden Schritten nach und nach ergänzen, bis die Pipeline vervollständigt wurde. Registrieren Sie zunächst wieder einen Runner für Ihr Projekt und überprüfen Sie die grundsätzliche Funktionalität. Deaktivieren Sie in den CI/CD Settings Ihres Projekts zusätzlich die Shared runners. Deaktivieren Sie dazu die Option Enable shared runners for this project.

# Schritt 3
Bevor wir den Code in sum.router.ts ergänzen, wollen wir noch sicherstellen, dass er stets korrekt formatiert wird. Dies soll in der Stage lint realisiert werden. Die eigentliche Formatierung wird von Prettier übernommen.
Erweitern Sie die Stage um die folgenden Punkte:

Installieren Sie alle benötigten Pakete. In diesem Projekt wird npm als Paketmanager genutzt. Sie kennen aus den vorherigen Praktika bereits Yarn, die Funktionalität ist in den meisten Punkten vergleichbar.
Finden Sie heraus, wie Sie Prettier aufrufen können. Alle benötigten Informationen sind in der package.json enthalten.
Testen Sie Ihre Pipeline und schauen Sie sich die Ergebnisse an.

```yaml
stages:
  - lint
  - test

Fix linting errors:
  stage: lint
  script:
    - |
      npm install --save-dev --save-exact prettier
      echo {}> .prettierrc.json
      npx prettier --write src/
        
Test application:
  stage: test
  script:
    - Inhalt
```

# Schritt 4
Mit der aktuellen Pipeline wird der Code lediglich lokal beim Gitlab-Runner formatiert. Falls Änderungen vorgenommen wurden, soll allerdings sofort ein Commit ins Repository erfolgen.
Fügen Sie der Stage die folgenden Schritte hinzu:

Überprüfen Sie zunächst, ob überhaupt Änderungen durchgeführt wurden. Falls keine Änderungen vorgenommen wurden, sind die folgenden Schritte nicht durchzuführen. An dieser Stelle ist es sinnvoll sich mit Multiline Blocks zu befassen.
Erstellen Sie einen Commit mit einer geeigneten Commit-Nachricht. Überlegen Sie, ob die Option -a an dieser Stelle sinnvoll ist.

Unter Umständen müssen Sie hier noch git konfigurieren und eine Mail-Adresse sowie den Benutzernamen setzen.


Pushen Sie die Änderungen auf den HEAD des aktuellen Branches.

Hier werden Sie vermutlich Probleme bekommen, da Gitlab die Eingabe von Zugangsdaten erwartet. Um das Problem zu lösen, müssen die folgenden Schritte durchgeführt werden:

Erstellen Sie sich ein Access Token. Dies ist unter Settings -> Access Tokens möglich. Vergeben Sie einen geeigneten Namen für den resultierenden Bot und setzen Sie den Haken bei write_repository. Wählen Sie eine geeignete Rolle für das Token. Achtung: Sie müssen das Token kopieren, sobald die Seite verlassen wurde ist es für immer verloren!
Wechseln Sie nun zu Settings -> CI/CD -> Variables und fügen Sie eine Variable hinzu. Sie können diese z.B. PROJECT_ACCESS_TOKEN nennen, fügen Sie das kopierte Token als Value ein. Ist es sinnvoll die Variable zu schützen oder zu maskieren? Treffen Sie eine Entscheidung!
Erweitern Sie Ihren Push-Befehl gemäß dieser Anleitung.




Der erneute Commit wird die Pipeline erneut triggern. Ist das ein erwünschtes Verhalten? Begründen Sie Ihre Antwort!
Schicken Sie einen Exit-Code, der die Pipeline als fehlerhaft beendet.

Sollten Sie Inspiration benötigen, ist u.a. dieser Blog-Post interessant.
Testen Sie abschließend Ihre Änderungen!

```yaml
stages:
  - lint
  - test

Fix linting errors:
  stage: lint
  script:
    - |
      npm install --save-dev --save-exact prettier
      echo {}> .prettierrc.json
      npx prettier --write src/
  after_script:
    - |
      CHANGES=$(git status --porcelain | wc -l)
      if [ "$CHANGES" != "0" ]; then
        git config --global user.email "PLBot@pipeline"
        git config --global user.name "PLBot"
        git add .
        git commit -a -m "Pushed from PL"
        git push "https://gitlab-ci-token:$PROJECT_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git" HEAD:$CI_COMMIT_BRANCH
        echo "In Git Gepushed"
        exit 1
      else
        echo "Keine Änderungen"
        exit 0
      fi    
    
        
Test application:
  stage: test
  script:
    - Inhalt
```

# Schritt 5
Jetzt muss die Stage test erweitert werden. In diesem Fall sind die folgenden Schritte durchzuführen:

Führen Sie die Tests aus. Auch hier finden Sie alle benötigten Informationen in der package.json. Setzen Sie sich darüber hinaus mit npm clean-install auseinander und entscheiden Sie, ob eine Verwendung hier angebracht ist.
Im Zuge der Tests erhalten wir zusätzlich zu den Tests einen Code-Coverage-Bericht. Dieser soll in das Gitlab-Projekt eingebunden werden. Benutzen Sie die folgenden Quellen: Gitlab Doku & Gitlab Forum

Unter Settings -> CI/CD -> General pipelines finden Sie den Coverage Report und den Pipeline Status. Binden Sie diesen in das Readme des Projekts ein.

Testen Sie auch hier wieder die erweiterte Funktionalität!

```yaml
stages:
  - lint
  - test

Fix linting errors:
  stage: lint
  script:
    - |
      npm install --save-dev --save-exact prettier
      echo {}> .prettierrc.json
      npx prettier --write src/
  after_script:
    - |
      CHANGES=$(git status --porcelain | wc -l)
      if [ "$CHANGES" != "0" ]; then
        git config --global user.email "PLBot@pipeline"
        git config --global user.name "PLBot"
        git add .
        git commit -a -m "Pushed from PL"
        git push "https://gitlab-ci-token:$PROJECT_ACCESS_TOKEN@$CI_SERVER_HOST/$CI_PROJECT_PATH.git" HEAD:$CI_COMMIT_BRANCH
        echo "In Git Gepushed"
        exit 1
      else
        echo "Keine Änderungen"
        exit 0
      fi    
    
        
Test application:
  stage: test
  script:
    - npm clean-install
    - npm run test
  coverage: /All\sfiles.*?\s+(\d+.\d+)/

```

# Schritt 6
Jetzt haben Sie die Grundlage für die testgetriebene Entwicklung geschaffen. Vervollständigen Sie sum.router.ts und überprüfen Sie mit Hilfe der Pipeline Ihren Fortschritt.

```ts
import { Router } from "express";

export const router = Router();

router.get("/sum/:a/:b", (req, res) => {
  // TODO
  // This line is needed to run tests properly
  let a = parseInt(req.params.a);
  let b = parseInt(req.params.b);

  if (a != undefined && b != undefined) {
    if (!isNaN(a) && !isNaN(b)) {
      res.status(200).send({ msg: `Sum: ${a + b}` });
    } else {
      res.status(200).send({ msg: `Sum: ${"NaN"}` });
    }
  } else {
    res.status(400).send({ msg: `You have to edit this file!` });
  }
});
```